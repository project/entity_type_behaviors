<?php

/**
 * @file
 * Hooks specific to the Entity type behaviors module.
 */

use Drupal\entity_type_behaviors\EntityTypeBehaviorInterface;

/**
 * Allows a user to preprocess a specific behavior for all entity types.
 *
 * The hook does not take into account the entity type.
 * The hook does not take into account the bundle.
 *
 * @param array $build
 *   An array containing the preprocess variables for an entity.
 * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface $behavior
 *   The requested behavior plugin for that entity.
 */
function hook_entity_type_behaviors_alter__BEHAVIOR(
  array &$build,
  EntityTypeBehaviorInterface $behavior,
) {
  $values = $behavior->getValues();

  if (!empty($values['bg_color'])) {
    $build['#attributes']['class'][] = 'test';
  }
}

/**
 * Allows a user to preprocess a specific behavior and an entity type.
 *
 * The hook does not take into account the bundle.
 *
 * @param array $build
 *   An array containing the preprocess variables for an entity.
 * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface $behavior
 *   The requested behavior plugin for that entity.
 */
function hook_entity_type_behaviors_alter__BEHAVIOR__ENTITY_TYPE(
  array &$build,
  EntityTypeBehaviorInterface $behavior,
) {
  $values = $behavior->getValues();

  if (!empty($values['bg_color'])) {
    $build['#attributes']['class'][] = 'test-entity-type';
  }
}

/**
 * Allows a user to preprocess a specific behavior, an entity type and a bundle.
 *
 * @param array $build
 *   An array containing the preprocess variables for an entity.
 * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface $behavior
 *   The requested behavior plugin for that entity.
 */
function hook_entity_type_behaviors_alter__BEHAVIOR__ENTITY_TYPE__BUNDLE(
  array &$build,
  EntityTypeBehaviorInterface $behavior,
) {
  $values = $behavior->getValues();

  if (!empty($values['bg_color'])) {
    $build['#attributes']['class'][] = 'test-entity-type';
  }
}
