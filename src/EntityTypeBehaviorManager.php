<?php

namespace Drupal\entity_type_behaviors;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_type_behaviors\Annotation\EntityTypeBehavior;

/**
 * The entity type behavior manager class.
 *
 * @package Drupal\entity_type_behaviors
 */
class EntityTypeBehaviorManager extends DefaultPluginManager {

  /**
   * Constructs a ParagraphsBehaviorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/EntityTypeBehavior',
      $namespaces,
      $module_handler,
      EntityTypeBehaviorInterface::class,
      EntityTypeBehavior::class
    );

    $this->setCacheBackend($cache_backend, 'entity_type_behavior_plugins');
    $this->alterInfo('entity_type_behavior_info');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    uasort($definitions, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $definitions;
  }

  /**
   * Gets the definitions by entity type.
   *
   * @param string $entityType
   *   The entity type.
   *
   * @return array
   *   An array containing the definitions by entity type.
   */
  public function getDefinitionsByEntityType(string $entityType) : array {
    $definitions = $this->getDefinitions();

    if (empty($definitions)) {
      return [];
    }

    $matchingDefinitions = [];

    foreach ($definitions as $id => $definition) {
      // Empty entity types array means this is available for all entity types.
      if (empty($definition['entityTypes']) || !is_array($definition['entityTypes'])) {
        $matchingDefinitions[$id] = $definition;
        continue;
      }

      if (!in_array($entityType, $definition['entityTypes'])) {
        continue;
      }

      $matchingDefinitions[$id] = $definition;
    }

    return $matchingDefinitions;
  }

}
