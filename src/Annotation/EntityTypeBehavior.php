<?php

namespace Drupal\entity_type_behaviors\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Entity type behavior annotation class.
 *
 * @package Drupal\entity_type_behaviors\Annotation
 *
 * @Annotation
 */
class EntityTypeBehavior extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the paragraphs behavior plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *   Allow translation of this label.
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var string
   *   The plugin description.
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

  /**
   * Limit this plugin to certain entity types.
   *
   * @var array
   */
  public $entityTypes;

}
