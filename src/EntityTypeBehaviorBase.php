<?php

namespace Drupal\entity_type_behaviors;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity type behaviors.
 *
 * @package Drupal\entity_type_behaviors
 */
abstract class EntityTypeBehaviorBase extends PluginBase implements EntityTypeBehaviorInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValues(): array {
    return $this->configuration['values'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValueByKey(string $key) {
    $values = $this->getValues();

    if (empty($values) || !isset($values[$key])) {
      return NULL;
    }

    return $values[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): array {
    return $this->configuration['config'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): ?string {
    return $this->configuration['entity_type'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundle(): ?string {
    return $this->configuration['bundle'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigValue(string $key) {
    $config = $this->getConfig();

    if (empty($config) || !isset($config[$key])) {
      return NULL;
    }

    return $config[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function massageValues(array $values, array $form, FormStateInterface $form_state) : array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function massageConfig(array $config, array $form, FormStateInterface $form_state) : array {
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    // This method does nothing. Using this method is not required.
  }

}
