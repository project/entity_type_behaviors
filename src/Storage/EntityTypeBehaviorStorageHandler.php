<?php

namespace Drupal\entity_type_behaviors\Storage;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class EntityTypeBehaviorStorage.
 *
 * This class helps us with installing the behaviors base field definition.
 *
 * @package Drupal\entity_type_behaviors\Helper
 */
class EntityTypeBehaviorStorageHandler {

  use StringTranslationTrait;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  private $entityDefinitionUpdateManager;

  /**
   * The entity last installed schema repository.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  private $entityLastInstalledSchemaRepository;

  /**
   * EntityTypeBehaviorStorageHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager
   *   The entity definition update manager.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository
   *   The entity last installed schema repository.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager,
    EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository,
  ) {
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->entityLastInstalledSchemaRepository = $entityLastInstalledSchemaRepository;
  }

  /**
   * The behavior base field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The base field definition for behaviors.
   */
  public function getBehaviorBaseField() : BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_type_behavior')
      ->setLabel($this->t('Behavior settings'))
      ->setName('behaviors')
      ->setTargetBundle(NULL)
      ->setTranslatable(TRUE)
      ->setDescription($this->t('The behavior plugin settings'))
      ->setRevisionable(TRUE)
      ->setDefaultValue([]);
  }

  /**
   * Handles the base field configuration installation and updating.
   *
   * @param string $entityTypeId
   *   The entity type.
   * @param array|null $sandbox
   *   The batch context.
   */
  public function updateBaseFieldConfiguration(string $entityTypeId, ?array &$sandbox = NULL) {
    $entity_type = $this->entityDefinitionUpdateManager->getEntityType($entityTypeId);

    $field_storage_definitions = $this->entityLastInstalledSchemaRepository
      ->getLastInstalledFieldStorageDefinitions($entityTypeId);

    // The behavior field has already been installed.
    if (isset($field_storage_definitions['behaviors'])) {
      $sandbox['#finished'] = 1;
      return;
    }

    $field_storage_definitions['behaviors'] = $this->getBehaviorBaseField()
      ->setTargetEntityTypeId($entityTypeId);

    $this->entityDefinitionUpdateManager
      ->updateFieldableEntityType($entity_type, $field_storage_definitions, $sandbox);
  }

  /**
   * Removes the base field configuration.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param array|null $sandbox
   *   A sandbox.
   */
  public function removeBaseFieldConfiguration(string $entityTypeId, ?array &$sandbox = NULL) {
    $field_storage_definitions = $this->entityLastInstalledSchemaRepository
      ->getLastInstalledFieldStorageDefinitions($entityTypeId);

    // The behavior field has already been installed.
    if (!isset($field_storage_definitions['behaviors'])) {
      $sandbox['#finished'] = 1;
      return;
    }

    $behaviorField = $this->getBehaviorBaseField()->setTargetEntityTypeId($entityTypeId);
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($behaviorField);
  }

}
