<?php

namespace Drupal\entity_type_behaviors\Config;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * The behavior config factory.
 *
 * @package Drupal\entity_type_behaviors\Config
 */
class BehaviorConfigFactory {

  const CONFIG_PREFIX = 'entity_type_behaviors.entity_type_bundle';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * BehaviorConfigFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Stores the behavior config for a particular entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   * @param array $behaviors
   *   The behaviors for that entity type and bundle.
   *
   * @return \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   *   The current BehaviorConfigFactory
   */
  public function saveBehaviors(string $entityTypeId, string $bundle, array $behaviors) : BehaviorConfigFactory {
    $editable = $this->getEditable($entityTypeId, $bundle);

    foreach ($behaviors as &$behavior) {
      if (empty($behavior['config'])) {
        $behavior['config'] = [];
      }

      $behavior['config'] = serialize($behavior['config']);
    }

    $this->configFactory->getEditable($editable)
      ->set('behaviors', $behaviors)->save();

    return $this;
  }

  /**
   * Returns all configured entity types and bundles in one array.
   *
   * @return array
   *   An array with entity types and bundles that have enabled behaviors.
   */
  public function getConfiguredEntityTypesAndBundles() : array {
    $prefix = sprintf('%s.', static::CONFIG_PREFIX);
    $configNames = $this->configFactory->listAll($prefix);

    if (empty($configNames)) {
      return [];
    }

    $entityTypeBundles = [];

    foreach ($configNames as $configName) {
      if (!$entityTypeAndBundle = $this->getEntityTypeAndBundleFromEditable($configName)) {
        continue;
      }

      $entityType = $entityTypeAndBundle[0];
      $bundle = $entityTypeAndBundle[1];

      // Check if there are enabled behaviors.
      $enabledBehaviors = $this->getEnabledConfiguredBehaviors($entityType, $bundle);

      if (empty($enabledBehaviors)) {
        continue;
      }

      $entityTypeBundles[$entityType][] = $bundle;
    }

    return $entityTypeBundles;
  }

  /**
   * Returns the configured bundles that have behaviors by entity type.
   *
   * @param string $entityTypeId
   *   The entity type id.
   *
   * @return array
   *   An array containing all bundles by entity type.
   */
  public function getConfiguredBundlesByEntityType(string $entityTypeId) : array {
    $bundlesByEntityType = $this->getConfiguredEntityTypesAndBundles();

    if (empty($bundlesByEntityType) || empty($bundlesByEntityType[$entityTypeId])) {
      return [];
    }

    return $bundlesByEntityType[$entityTypeId];
  }

  /**
   * Determines the entity type from the editable.
   */
  public function getEntityTypeFromEditable(string $editable) : ?string {
    if (!$entityTypeAndBundle = $this->getEntityTypeAndBundleFromEditable($editable)) {
      return NULL;
    }

    return $entityTypeAndBundle[0];
  }

  /**
   * Returns a list containing the entity type and the bundle.
   *
   * Entity type is key 0.
   * Bundle is key 1.
   *
   * Returns NULL if entity type or bundle could not be matched.
   *
   * @param string $editable
   *   The name of the config.
   *
   * @return array|null
   *   The entity type and bundle.
   */
  public function getEntityTypeAndBundleFromEditable(string $editable) : ?array {
    $prefix = sprintf('%s.', static::CONFIG_PREFIX);
    $entityTypeBundle = str_replace($prefix, '', $editable);
    $pieces = explode('.', $entityTypeBundle);

    if (empty($pieces[0]) || empty($pieces[1])) {
      return NULL;
    }

    return $pieces;
  }

  /**
   * Returns the enabled configured behaviors for an entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   The configured behaviors.
   */
  public function getEnabledConfiguredBehaviors(string $entityTypeId, string $bundle) : array {
    $behaviors = $this->getConfiguredBehaviors($entityTypeId, $bundle);

    if (empty($behaviors)) {
      return [];
    }

    foreach ($behaviors as $key => $behavior) {
      if (empty($behavior['enable'])) {
        unset($behaviors[$key]);
      }
    }

    return $behaviors;
  }

  /**
   * Returns the configured behaviors for an entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   The configured behaviors.
   */
  public function getConfiguredBehaviors(string $entityTypeId, string $bundle) : array {
    $data = $this->getConfigDataForEntityTypeAndBundle($entityTypeId, $bundle);
    return $data['behaviors'] ?? [];
  }

  /**
   * Gets behavior config for entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\Core\Config\Config|null
   *   The config if found, else NULL.
   */
  public function getConfigForEntityTypeAndBundle(string $entityTypeId, string $bundle) : ?Config {
    $editable = $this->getEditable($entityTypeId, $bundle);
    $config = $this->configFactory->getEditable($editable);

    if (!$config) {
      return NULL;
    }

    return $config;
  }

  /**
   * Returns config data for entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return array|null
   *   An array containing the data.
   */
  public function getConfigDataForEntityTypeAndBundle(string $entityTypeId, string $bundle) : ?array {
    if (!$config = $this->getConfigForEntityTypeAndBundle($entityTypeId, $bundle)) {
      return NULL;
    }

    $configData = $config->getRawData();

    if (empty($configData)) {
      return [];
    }

    if (!empty($configData['behaviors'])) {
      foreach ($configData['behaviors'] as $key => &$behavior) {
        if (!$behavior['enable']) {
          unset($configData['behaviors'][$key]);
        }

        if (!isset($behavior['config']) || !is_string($behavior['config'])) {
          continue;
        }

        $behavior['config'] = unserialize($behavior['config'], ['allowed_classes' => FALSE]);
      }
    }

    return $configData;
  }

  /**
   * Removes behaviors for an entity type id and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   */
  public function removeBehaviors(string $entityTypeId, string $bundle) {
    $editable = $this->getConfigForEntityTypeAndBundle($entityTypeId, $bundle);

    if (!$editable) {
      return;
    }

    $editable->delete();
  }

  /**
   * Removes all behavior configs.
   *
   * This is used on the hook_uninstall.
   */
  public function removeBehaviorConfigs() {
    $prefix = sprintf('%s.', static::CONFIG_PREFIX);
    $configNames = $this->configFactory->listAll($prefix);

    if (empty($configNames)) {
      return;
    }

    foreach ($configNames as $configName) {
      $config = $this->configFactory->getEditable($configName);

      if (!$config instanceof Config) {
        continue;
      }

      $config->delete();
    }
  }

  /**
   * Determines if the config name is a behavior config.
   *
   * @param string $editableName
   *   The config name.
   *
   * @return bool
   *   Whether or nog the config name is a behavior config.
   */
  public function isBehaviorConfig(string $editableName) : bool {
    return (strpos(static::CONFIG_PREFIX, $editableName) !== 0);
  }

  /**
   * Checks if the config exists for an entity type and bundle.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return bool
   *   Does the config exist?
   */
  public function configExists(string $entityTypeId, string $bundle) : bool {
    return (bool) $this->getConfigForEntityTypeAndBundle($entityTypeId, $bundle);
  }

  /**
   * Returns the editable name.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $bundle
   *   The bundle.
   *
   * @return string
   *   The editable name.
   */
  protected function getEditable(string $entityTypeId, string $bundle) {
    return sprintf('%s.%s.%s', static::CONFIG_PREFIX, $entityTypeId, $bundle);
  }

}
