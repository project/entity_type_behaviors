<?php

namespace Drupal\entity_type_behaviors\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_type_behaviors\Config\BehaviorConfigFactory;
use Drupal\entity_type_behaviors\EntityTypeBehaviorManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityTypeBehaviorWidget.
 *
 * The main widget for our entity_type_behavior field type.
 *
 * @package Drupal\entity_type_behaviors\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "entity_type_behavior_default",
 *   label = @Translation("Entity Type Behavior Default"),
 *   field_types = {
 *    "entity_type_behavior"
 *   }
 * )
 */
class EntityTypeBehaviorWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The behavior plugin manager.
   *
   * @var \Drupal\entity_type_behaviors\EntityTypeBehaviorManager
   *   The behavior plugin manager.
   */
  private $behaviorManager;

  /**
   * The config factory.
   *
   * @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   *   The config factory.
   */
  private $behaviorConfigFactory;

  /**
   * EntityTypeBehaviorWidget constructor.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The settings array.
   * @param array $third_party_settings
   *   The third party settings.
   * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorManager $behaviorManager
   *   The behavior plugin manager.
   * @param \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $behaviorConfigFactory
   *   The config factory.
   */
  public function __construct(
    string $plugin_id,
    array $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeBehaviorManager $behaviorManager,
    BehaviorConfigFactory $behaviorConfigFactory,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->behaviorManager = $behaviorManager;
    $this->behaviorConfigFactory = $behaviorConfigFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.entity_type_behavior'),
      $container->get('entity_type_behaviors.config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $behaviors = $this->getFieldSetting('behaviors');

    if (empty($behaviors)) {
      return [];
    }

    $defaultValues = $items->get($delta)->getValue();

    $element['value'] = [
      '#tree' => TRUE,
      '#type' => 'container',
    ];

    foreach ($behaviors as $id => $behavior) {
      $element['value'][$id] = [
        '#type' => 'details',
        '#tree' => TRUE,
      ];

      try {
        $plugin = $this->behaviorManager->createInstance($id, [
          'entity_type' => $this->fieldDefinition->getTargetEntityTypeId(),
          'bundle' => $this->fieldDefinition->getTargetBundle(),
          'config' => $behavior['config'] ?? [],
          'values' => $defaultValues['value'][$id] ?? [],
        ]);

        $pluginDefinition = $plugin->getPluginDefinition();

        $title = $pluginDefinition['label'] ?? '';
        $element['value'][$id]['#title'] = $title;

        $weight = $pluginDefinition['weight'] ?? 0;
        $element['value'][$id]['#weight'] = $weight;

        $pluginForm = $plugin->getForm();
      }
      catch (\Exception $e) {
        $pluginForm = [
          '#markup' => $this->t('The @id behavior caused an error', [
            '@id' => $id,
          ]),
        ];
      }

      $element['value'][$id] += $pluginForm ?: ['#access' => FALSE];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    $behaviorConfig = $this->getFieldSetting('behaviors');

    foreach ($values as &$behaviors) {
      if (empty($behaviors['value'])) {
        continue;
      }

      foreach ($behaviors['value'] as $id => &$behaviorValues) {
        if (!$this->behaviorManager->hasDefinition($id)) {
          continue;
        }

        $plugin = $this->behaviorManager->createInstance($id, [
          'entity_type' => $this->fieldDefinition->getTargetEntityTypeId(),
          'bundle' => $this->fieldDefinition->getTargetBundle(),
          'config' => $behaviorConfig[$id]['config'] ?? [],
        ]);

        $behaviorValues = $plugin->massageValues($behaviorValues, $form, $form_state);
      }
    }

    return $values;
  }

}
