<?php

namespace Drupal\entity_type_behaviors\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'entity_type_behavior' field type.
 *
 * @FieldType(
 *   id = "entity_type_behavior",
 *   label = @Translation("Entity Type Behavior"),
 *   description = @Translation("This field manages entity type behavior configuration."),
 *   default_widget = "entity_type_behavior_default",
 *   no_ui = TRUE
 * )
 */
class EntityTypeBehaviorItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'behaviors' => [],
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = MapDataDefinition::create()
      ->setLabel(new TranslatableMarkup('Value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $keys = array_keys($this->definition->getPropertyDefinitions());
    $mainKey = $keys[0];

    if (is_array($values) && !isset($values[$mainKey])) {
      $values = [$mainKey => $values];
    }

    parent::setValue($values, $notify);
  }

}
