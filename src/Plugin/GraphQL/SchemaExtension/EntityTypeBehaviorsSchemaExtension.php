<?php

declare(strict_types=1);

namespace Drupal\entity_type_behaviors\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql_compose\Plugin\GraphQL\SchemaExtension\ResolverOnlySchemaExtensionPluginBase;

/**
 * Entity type behaviors GraphQL Extension.
 *
 * @SchemaExtension(
 *   id = "entity_type_behaviors_schema_extension",
 *   name = "GraphQL Compose Entity Type Behaviors",
 *   description = "Entity type behaviors",
 *   schema = "graphql_compose"
 * )
 */
class EntityTypeBehaviorsSchemaExtension extends ResolverOnlySchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();
    $bundles = [];
    $paragraph_plugin = $this->gqlEntityTypeManager->getPluginInstance('paragraph');
    $node_plugin = $this->gqlEntityTypeManager->getPluginInstance('node');
    if (!$paragraph_plugin && !$node_plugin) {
      return;
    }

    $bundles = array_merge($paragraph_plugin->getBundles(), $node_plugin->getBundles());

    foreach ($bundles as $bundle) {
      $registry->addFieldResolver(
        $bundle->getTypeSdl(),
        'entity_type_behaviors',
        $builder->produce('entity_type_behaviors')
          ->map('entity', $builder->fromParent())
      );
    }
  }

}
