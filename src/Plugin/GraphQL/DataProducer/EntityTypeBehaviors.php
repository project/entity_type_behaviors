<?php

declare(strict_types=1);

namespace Drupal\entity_type_behaviors\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_type_behaviors\Helper\EntityTypeBehaviorHelper;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Get the Paragraph if it has Layout Paragraphs settings.
 *
 * @DataProducer(
 *   id = "entity_type_behaviors",
 *   name = @Translation("Entity Type behaviors"),
 *   description = @Translation("Entity Type behaviors information for Paragraph entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Entity Type behaviors settings")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *     ),
 *   }
 * )
 */
class EntityTypeBehaviors extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Entity type behavior helper.
   *
   * @var \Drupal\entity_type_behaviors\Helper\EntityTypeBehaviorHelper
   */
  protected $entityTypeBehaviorHelper;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create($container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type_behaviors.helper.entity_type_behavior')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param array $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\entity_type_behaviors\Helper\EntityTypeBehaviorHelper $entityTypeBehaviorHelper
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeBehaviorHelper $entityTypeBehaviorHelper,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeBehaviorHelper = $entityTypeBehaviorHelper;
  }

  /**
   * Return the entity if it has Entity type behaviors setting.
   *
   * @param mixed $entity
   *   The paragraph entity.
   *
   * @return array|null
   *   The layout paragraphs settings.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function resolve(mixed $entity): ?array {
    if (!$entity) {
      return NULL;
    }
    $behaviors = $this->entityTypeBehaviorHelper->getBehaviorValuesForEntity($entity);
    $result = [];
    foreach ($behaviors as $key => $behavior) {
      $result[$key] = $behavior[$key];
    }

    return $result;
  }

}
