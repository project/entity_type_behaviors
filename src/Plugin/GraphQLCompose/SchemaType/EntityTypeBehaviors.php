<?php

declare(strict_types=1);

namespace Drupal\entity_type_behaviors\Plugin\GraphQLCompose\SchemaType;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_type_behaviors\EntityTypeBehaviorManager;
use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeSchemaTypeBase;
use Drupal\graphql_compose\Plugin\GraphQLComposeEntityTypeManager;
use Drupal\graphql_compose\Plugin\GraphQLComposeFieldTypeManager;
use Drupal\graphql_compose\Plugin\GraphQLComposeSchemaTypeManager;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 *
 * @GraphQLComposeSchemaType(
 *   id = "EntityTypeBehaviors"
 * )
 */
class EntityTypeBehaviors extends GraphQLComposeSchemaTypeBase {

  /**
   * The behavior plugin manager.
   *
   * @var \Drupal\entity_type_behaviors\EntityTypeBehaviorManager
   *   The behavior plugin manager.
   */
  private $behaviorManager;

  /**
   * Entity type behaviors schema type constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition array.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Drupal entity type manager.
   * @param \Drupal\graphql_compose\Plugin\GraphQLComposeEntityTypeManager $gqlEntityTypeManager
   *   GraphQL Compose entity type plugin manager.
   * @param \Drupal\graphql_compose\Plugin\GraphQLComposeFieldTypeManager $gqlFieldTypeManager
   *   GraphQL Compose field type plugin manager.
   * @param \Drupal\graphql_compose\Plugin\GraphQLComposeSchemaTypeManager $gqlSchemaTypeManager
   *   GraphQL Compose schema type plugin manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Drupal language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Drupal module handler.
   * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorManager $behaviorManager
   *   The entity type behavior manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected GraphQLComposeEntityTypeManager $gqlEntityTypeManager,
    protected GraphQLComposeFieldTypeManager $gqlFieldTypeManager,
    protected GraphQLComposeSchemaTypeManager $gqlSchemaTypeManager,
    protected LanguageManagerInterface $languageManager,
    protected ModuleHandlerInterface $moduleHandler,
    EntityTypeBehaviorManager $behaviorManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory, $entityTypeManager, $gqlEntityTypeManager, $gqlFieldTypeManager, $gqlSchemaTypeManager, $languageManager, $moduleHandler);
    $this->behaviorManager = $behaviorManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('graphql_compose.entity_type_manager'),
      $container->get('graphql_compose.field_type_manager'),
      $container->get('graphql_compose.schema_type_manager'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('plugin.manager.entity_type_behavior')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTypes(): array {
    $types = [];
    $definitions = $this->behaviorManager->getDefinitions();

    $fields = [];
    foreach ($definitions as $definition) {
      $fields[$definition['id']] = [
        'type' => Type::string(),
      ];

      if ($definition['description'] instanceof TranslatableMarkup) {
        // phpcs:ignore
        $fields[$definition['id']]['description'] = (string) $this->t(($definition['description']->getUntranslatedString()));
      }
      else {
        $fields[$definition['id']]['description'] = "";
      }
    }

    $types[] = new ObjectType([
      'name' => $this->getPluginId(),
      'description' => (string) $this->t('The entity type behaviors for paragraphs'),
      'fields' => fn() =>
      $fields,
    ]);

    return $types;
  }

}
