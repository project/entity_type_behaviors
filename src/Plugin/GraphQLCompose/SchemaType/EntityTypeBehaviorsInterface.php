<?php

declare(strict_types=1);

namespace Drupal\entity_type_behaviors\Plugin\GraphQLCompose\SchemaType;

use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeEntityTypeInterface;
use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeSchemaTypeBase;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\ObjectType;

/**
 * {@inheritdoc}
 *
 * @GraphQLComposeSchemaType(
 *   id = "EntityTypeBehaviorsInterface",
 * )
 */
class EntityTypeBehaviorsInterface extends GraphQLComposeSchemaTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getTypes(): array {
    $types = [];

    $types[] = new InterfaceType([
      'name' => $this->getPluginId(),
      'description' => (string) $this->t('This content has been arranged by a User using a layout builder.'),
      'fields' => fn() => [
        'entity_type_behaviors' => static::type('EntityTypeBehaviors'),
      ],
    ]);

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensions(): array {
    $extensions = parent::getExtensions();

    $paragraph_plugin = $this->gqlEntityTypeManager->getPluginInstance('paragraph');
    $node_plugin = $this->gqlEntityTypeManager->getPluginInstance('node');

    if ($paragraph_plugin instanceof GraphQLComposeEntityTypeInterface) {
      $extensions = array_merge($extensions, $this->getExtensionsByPlugin($paragraph_plugin));
    }

    if ($node_plugin instanceof GraphQLComposeEntityTypeInterface) {
      $extensions = array_merge($extensions, $this->getExtensionsByPlugin($node_plugin));
    }

    return $extensions;
  }

  /**
   * Get extensions for bundles.
   */
  public function getExtensionsByPlugin(GraphQLComposeEntityTypeInterface $plugin): array {
    $extensions = [];
    $bundles = $plugin->getBundles();
    foreach ($bundles as $bundle) {
      $extensions[] = new ObjectType([
        'name' => $bundle->getTypeSdl(),
        'interfaces' => fn() => [
          static::type('EntityTypeBehaviorsInterface'),
        ],
        'fields' => fn() => [
          'entity_type_behaviors' => [
            'type' => static::type('EntityTypeBehaviors'),
            'description' => (string) $this->t('Behaviors for this paragraph.'),
          ],
        ],
      ]);
    }
    return $extensions;
  }

}
