<?php

namespace Drupal\entity_type_behaviors;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for entity type behaviors.
 *
 * @package Drupal\entity_type_behaviors
 */
interface EntityTypeBehaviorInterface {

  /**
   * Returns the form for the entity type behavior.
   *
   * @return array
   *   The form element.
   */
  public function getForm() : array;

  /**
   * Gets the config form.
   *
   * @return array
   *   The config form.
   */
  public function getConfigForm() : array;

  /**
   * Massages the config before storing them.
   *
   * @param array $values
   *   An array containing the current config values.
   * @param array $form
   *   An array containing the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The config values.
   */
  public function massageValues(array $values, array $form, FormStateInterface $form_state) : array;

  /**
   * Massages the config before storing them.
   *
   * @param array $config
   *   An array containing the current config values.
   * @param array $form
   *   An array containing the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The config values.
   */
  public function massageConfig(array $config, array $form, FormStateInterface $form_state) : array;

  /**
   * Returns the default config.
   *
   * @return array
   *   An array containing default config.
   */
  public function getConfig() : array;

  /**
   * Gets a config value based on a key.
   *
   * @param string $key
   *   The key of the default config value.
   *
   * @return mixed
   *   The config value for that key.
   */
  public function getConfigValue(string $key);

  /**
   * Returns the default values.
   *
   * @return array
   *   An array containing default value.s
   */
  public function getValues() : array;

  /**
   * Gets a value value based on a key.
   *
   * @param string $key
   *   The key of the default value value.
   *
   * @return mixed
   *   The value for that key.
   */
  public function getValueByKey(string $key);

  /**
   * Returns the entity type id for which this plugin is configured.
   *
   * @return string|null
   *   The entity type id if set.
   */
  public function getEntityTypeId(): ?string;

  /**
   * Returns the bundle for which this plugin is configured.
   *
   * @return string|null
   *   The bundle if set.
   */
  public function getBundle(): ?string;

  /**
   * Extends the entity render array with behavior.
   *
   * @param array &$build
   *   A render array representing the entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to alter the view.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   Holds the display options configured for the entity component.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode);

}
