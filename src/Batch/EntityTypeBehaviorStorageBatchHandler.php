<?php

namespace Drupal\entity_type_behaviors\Batch;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler;

/**
 * Class EntityTypeBehaviorStorageBatchHandler.
 *
 * Wraps our base field storage functionality in a batch.
 * When a base field is dynamically added to an entity:
 * - All entities are updated.
 * - If there are more than 50, it uses batch to update.
 *
 * @package Drupal\entity_type_behaviors\Batch
 */
class EntityTypeBehaviorStorageBatchHandler {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The storage handler updates our entity's base field definition.
   *
   * @var \Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler
   */
  protected $storageHandler;

  /**
   * EntityTypeBehaviorStorageBatchHandler constructor.
   *
   * @param \Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler $storageHandler
   *   The storage handler updates our entity's base field definition.
   */
  public function __construct(EntityTypeBehaviorStorageHandler $storageHandler) {
    $this->storageHandler = $storageHandler;
  }

  /**
   * Generates a batch with multiple operations by entity types.
   *
   * @param array $entityTypes
   *   An array containing entity types.
   *
   * @return array
   *   An array containing the batch operations.
   */
  public function generateBatch(array $entityTypes) : array {
    $operations = [];

    foreach ($entityTypes as $entityType) {
      $operations[] = [
        [$this, 'batchOperation'],
        [$entityType],
      ];
    }

    return [
      'title' => $this->t('Starting behavior storage field update'),
      'operations' => $operations,
      'finished' => [$this, 'finishCallback'],
    ];
  }

  /**
   * Starts the entity type storage update batch.
   *
   * @param string $entityType
   *   The entity type to update.
   * @param bool $process
   *   If true, returns the batch process response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   The redirect response.
   */
  public function startStorageBatchProcessForEntityType(string $entityType, $process = TRUE) {
    $batch = $this->generateBatch([$entityType]);
    batch_set($batch);

    if ($process) {
      return batch_process();
    }
  }

  /**
   * The batch operation.
   *
   * @param string $entityType
   *   The entity type.
   * @param array $context
   *   The batch context.
   */
  public function batchOperation(string $entityType, array &$context) {
    $this->storageHandler->updateBaseFieldConfiguration($entityType, $context['sandbox']);

    if ($context['sandbox']['#finished'] !== 1) {
      $context['finished'] = $context['sandbox']['#finished'];
      $context['success'] = FALSE;
    }
    else {
      $context['success'] = TRUE;
    }
  }

  /**
   * Processes our base field configuration for a config sync step.
   *
   * @param array $entityTypes
   *   Array containing entity types to update.
   * @param array $context
   *   The context of the batch.
   */
  public function configStepOperation(array $entityTypes, &$context) {
    foreach ($entityTypes as $entityType) {
      $context[$entityType]['sandbox']['#finished'] = 0;

      while ($context[$entityType]['sandbox']['#finished'] !== 1) {
        $this->storageHandler->updateBaseFieldConfiguration($entityType, $context[$entityType]['sandbox']);
      }
    }

    $context['success'] = TRUE;
    $context['finished'] = 1;
  }

}
