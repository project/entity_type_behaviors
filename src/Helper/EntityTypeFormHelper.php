<?php

namespace Drupal\entity_type_behaviors\Helper;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_type_behaviors\Batch\EntityTypeBehaviorStorageBatchHandler;
use Drupal\entity_type_behaviors\Config\BehaviorConfigFactory;
use Drupal\entity_type_behaviors\EntityTypeBehaviorManager;

/**
 * Class EntityTypeFormHelper.
 *
 * This class helps us with attaching the behavior configuration field.
 * It checks if we are on a entity type edit form.
 * It checks if there are behaviors for that entity type.
 * It adds the configuration form.
 *
 * @package Drupal\entity_type_behaviors\Helper
 */
class EntityTypeFormHelper {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type for which to alter the form.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase|null
   */
  protected $entity = NULL;

  /**
   * The entity type id for which to alter the form.
   *
   * @var string|null
   */
  protected $entityTypeId = NULL;

  /**
   * The entity type for which to alter the form.
   *
   * @var string|null
   */
  protected $bundle = NULL;

  /**
   * The entity type behavior manager.
   *
   * @var \Drupal\entity_type_behaviors\EntityTypeBehaviorManager
   */
  protected $entityTypeBehaviorManager;

  /**
   * Helps us with storing and retrieving config.
   *
   * @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   */
  protected $configFactory;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The behavior storage batch handler.
   *
   * @var \Drupal\entity_type_behaviors\Batch\EntityTypeBehaviorStorageBatchHandler
   */
  private $behaviorStorageBatchHandler;

  /**
   * EntityTypeFormHelper constructor.
   *
   * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorManager $entityTypeBehaviorManager
   *   The entity type behavior manager.
   * @param \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $configFactory
   *   Helps us with storing and retrieving the behavior config.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager interface.
   * @param \Drupal\entity_type_behaviors\Batch\EntityTypeBehaviorStorageBatchHandler $behaviorStorageBatchHandler
   *   The batch handler.
   */
  public function __construct(
    EntityTypeBehaviorManager $entityTypeBehaviorManager,
    BehaviorConfigFactory $configFactory,
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeBehaviorStorageBatchHandler $behaviorStorageBatchHandler,
  ) {
    $this->entityTypeBehaviorManager = $entityTypeBehaviorManager;
    $this->configFactory = $configFactory;
    $this->entityFieldManager = $entityFieldManager;
    $this->behaviorStorageBatchHandler = $behaviorStorageBatchHandler;
  }

  /**
   * Processes a form.
   *
   * Checks if this is an entity type edit form.
   * Checks if there are behaviors for this entity type.
   * Allows enabling of the plugins.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function processForm(array &$form, FormStateInterface $form_state) {
    $this->processVariables($form_state);

    if (!$this->entityTypeId) {
      return;
    }

    if ($this->entity instanceof ConfigEntityBundleBase) {
      $this->attachBehaviorSettingsForm($form, $form_state);
    }
  }

  /**
   * Process and set the class variables.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function processVariables(FormStateInterface $form_state) {
    $formObject = $form_state->getFormObject();

    if (!$formObject instanceof EntityForm) {
      return;
    }

    $entity = $formObject->getEntity();

    if ($entity instanceof ConfigEntityBundleBase) {
      $this->entity = $entity;
      $this->entityTypeId = $entity->getEntityType()->getBundleOf();
      $this->bundle = $entity->id();
    }
  }

  /**
   * Attaches the behavior settings form.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function attachBehaviorSettingsForm(array &$form, FormStateInterface $form_state) {
    $definitions = $this->entityTypeBehaviorManager->getDefinitionsByEntityType($this->entityTypeId);

    if (empty($definitions)) {
      return;
    }

    // The bundle is not yet available on entity type creation forms.
    if ($this->bundle) {
      $configBehaviors = $this->configFactory->getConfiguredBehaviors($this->entityTypeId, $this->bundle);
    }
    else {
      $configBehaviors = [];
    }

    // Attach the behaviors.
    $form['behaviors'] = [
      '#type' => 'details',
      '#title' => $this->t('Behavior settings'),
      '#group' => 'additional_settings',
      '#tree' => TRUE,
    ];

    $form['behaviors']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable behaviors'),
      '#suffix' => $this->t('Enable behaviors for this entity type, might create or delete a base field.'),
      '#default_value' => !empty($configBehaviors) ? TRUE : FALSE,
    ];

    foreach ($definitions as $id => $definition) {
      $form['behaviors'][$id] = [
        '#type' => 'details',
        '#tree' => TRUE,
        '#title' => $definition['label'],
        '#states' => [
          'invisible' => [
            ':input[name="behaviors[enable]"]' => ['checked' => FALSE],
          ],
        ],
      ];

      $checkboxId = sprintf('behavior-checkbox--%s', $id);

      $form['behaviors'][$id]['enable'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable "@label" behavior', ['@label' => $definition['label']]),
        '#default_value' => $configBehaviors[$id]['enable'] ?? FALSE,
        '#id' => $checkboxId,
      ];

      /** @var \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface $plugin */
      $plugin = $this->entityTypeBehaviorManager->createInstance($id, [
        'entity_type' => $this->entityTypeId,
        'bundle' => $this->bundle,
        'config' => $configBehaviors[$id]['config'] ?? [],
      ]);

      $configForm = $plugin->getConfigForm();

      if (!empty($configForm)) {
        $form['behaviors'][$id]['config'] = [
          '#type' => 'container',
          '#tree' => TRUE,
          '#states' => [
            'invisible' => [
              ':input[name="behaviors[' . $id . '][enable]"]' => ['checked' => FALSE],
            ],
          ],
        ] + $configForm;
      }
    }

    $this->attachSubmitCallbackToForm($form, 'submitEntityTypeForm');
  }

  /**
   * Saves the behavior settings for an entity type.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitEntityTypeForm(array $form, FormStateInterface $form_state) {
    $behaviors = $form_state->getValue('behaviors');

    if (empty($behaviors)) {
      return;
    }

    $enable = $behaviors['enable'] ?? FALSE;
    $this->processVariables($form_state);

    // If enable has been turned off, and we have config. Remove it.
    // Also check if the base field should be removed.
    if (!$enable && $this->configFactory->configExists($this->entityTypeId, $this->bundle)) {
      $this->configFactory->removeBehaviors($this->entityTypeId, $this->bundle);
      return;
    }

    // We don't want to store the enable value in our configuration.
    unset($behaviors['enable']);

    foreach ($behaviors as $id => &$behavior) {
      if (!$this->entityTypeBehaviorManager->hasDefinition($id)) {
        continue;
      }

      /** @var \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface $plugin */
      $config = $behavior['config'] ?? [];

      if (empty($config)) {
        continue;
      }

      $plugin = $this->entityTypeBehaviorManager->createInstance($id);
      $behavior['config'] = $plugin->massageConfig($config, $form, $form_state);
    }

    $this->configFactory->saveBehaviors($this->entityTypeId, $this->bundle, $behaviors);

    $redirect = $this->behaviorStorageBatchHandler
      ->startStorageBatchProcessForEntityType($this->entityTypeId);

    $form_state->setResponse($redirect);
  }

  /**
   * Attaches the submit callback to the form.
   *
   * @param array $form
   *   An array containing the form.
   * @param string $string
   *   The method of this class that handles the submit.
   */
  protected function attachSubmitCallbackToForm(array &$form, string $string) {
    // Attach the submit callback.
    $submitCallback = [$this, $string];

    if (!empty($form['actions']['submit']['#submit'])) {
      $form['actions']['submit']['#submit'][] = $submitCallback;
    }
    else {
      $form['#submit'][] = $submitCallback;
    }
  }

}
