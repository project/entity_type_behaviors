<?php

namespace Drupal\entity_type_behaviors\Helper;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\entity_type_behaviors\Config\BehaviorConfigFactory;
use Drupal\entity_type_behaviors\EntityTypeBehaviorInterface;
use Drupal\entity_type_behaviors\EntityTypeBehaviorManager;

/**
 * Class EntityTypeBehaviorHelper.
 *
 * A helper class to help us with retrieving behaviors from an entity.
 * Used mainly in preprocess hooks.
 *
 * @package Drupal\entity_type_behaviors\Helper
 */
class EntityTypeBehaviorHelper {

  /**
   * The entity type behavior manager.
   *
   * @var \Drupal\entity_type_behaviors\EntityTypeBehaviorManager
   */
  protected $entityTypeBehaviorManager;

  /**
   * Helps us with storing and retrieving config.
   *
   * @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   */
  protected $configFactory;

  /**
   * EntityTypeFormHelper constructor.
   *
   * @param \Drupal\entity_type_behaviors\EntityTypeBehaviorManager $entityTypeBehaviorManager
   *   The entity type behavior manager.
   * @param \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $configFactory
   *   Helps us with storing and retrieving the behavior config.
   */
  public function __construct(
    EntityTypeBehaviorManager $entityTypeBehaviorManager,
    BehaviorConfigFactory $configFactory,
  ) {
    $this->entityTypeBehaviorManager = $entityTypeBehaviorManager;
    $this->configFactory = $configFactory;
  }

  /**
   * Gets the behavior plugins for an entity.
   *
   * Returns NULL if the entity is not compatible.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to return behaviors.
   *
   * @return \Drupal\entity_type_behaviors\EntityTypeBehaviorInterface[]|null
   *   An array containing behaviors.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown when a plugin can't be loaded.
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   Thrown when a plugin can't be loaded.
   */
  public function getBehaviorsForEntity(EntityInterface $entity) : ?array {
    $behaviorValues = $this->getBehaviorValuesForEntity($entity);

    if (empty($behaviorValues)) {
      return [];
    }

    $behaviorConfig = $this->configFactory->getConfigDataForEntityTypeAndBundle(
      $entity->getEntityTypeId(),
      $entity->bundle()
    );

    $behaviorPlugins = [];

    foreach ($behaviorValues as $id => $values) {
      if (!$this->entityTypeBehaviorManager->hasDefinition($id)) {
        continue;
      }

      $plugin = $this->entityTypeBehaviorManager->createInstance($id, [
        'entity_type' => $entity->getEntityTypeId(),
        'bundle' => $entity->bundle(),
        'values' => $values,
        'config' => $behaviorConfig['behaviors'][$id]['config'] ?? [],
      ]);

      if (!$plugin instanceof EntityTypeBehaviorInterface) {
        continue;
      }

      $behaviorPlugins[$id] = $plugin;
    }

    if (!is_array($behaviorPlugins)) {
      return [];
    }

    return $behaviorPlugins;
  }

  /**
   * Gets the behavior values for an entity.
   *
   * These are the unserialized behavior values stored for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to return behaviors.
   *
   * @return array|null
   *   An array containing behavior values.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   An exception thrown.
   */
  public function getBehaviorValuesForEntity(EntityInterface $entity) : ?array {
    $entityType = $entity->getEntityTypeId();

    $factory = $this->configFactory;
    $entityTypeBundles = $factory->getConfiguredEntityTypesAndBundles();

    if (empty($entityTypeBundles) || !isset($entityTypeBundles[$entityType])) {
      return NULL;
    }

    if (!$entity instanceof FieldableEntityInterface || !$entity->hasField('behaviors')) {
      return NULL;
    }

    $fieldList = $entity->get('behaviors');

    /** @var \Drupal\entity_type_behaviors\Plugin\Field\FieldType\EntityTypeBehaviorItem $behaviorsItem */
    if ($fieldList->isEmpty() || !$behaviorsItem = $fieldList->first()) {
      return [];
    }

    $values = $behaviorsItem->getValue();

    if (!is_array($values)) {
      return [];
    }

    $behaviors = $values['value'] ?? [];

    if (!is_array($behaviors)) {
      return [];
    }

    return $behaviors;
  }

}
