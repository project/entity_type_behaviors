<?php

namespace Drupal\entity_type_behaviors\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\entity_type_behaviors\Config\BehaviorConfigFactory;
use Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for behaviors.
 *
 * @package Drupal\entity_type_behaviors\EventSubscriber
 */
class BehaviorStorageConfigEventSubscriber implements EventSubscriberInterface {

  /**
   * The behavior config we are dealing with.
   *
   * The config is NULL if it's not a valid behavior config.
   *
   * @var \Drupal\Core\Config\Config|null
   */
  protected $config = NULL;

  /**
   * The entity type id from the config.
   *
   * @var string|null
   */
  protected $entityTypeId = NULL;

  /**
   * The bundle from the config.
   *
   * @var string|null
   */
  protected $bundle = NULL;

  /**
   * The behavior config factory.
   *
   * @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   */
  protected $configFactory;

  /**
   * The storage handler.
   *
   * @var \Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler
   */
  private $storageHandler;

  /**
   * BehaviorStorageConfigEventSubscriber constructor.
   *
   * @param \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $configFactory
   *   The behavior config factory.
   * @param \Drupal\entity_type_behaviors\Storage\EntityTypeBehaviorStorageHandler $storageHandler
   *   The storage handler.
   */
  public function __construct(
    BehaviorConfigFactory $configFactory,
    EntityTypeBehaviorStorageHandler $storageHandler,
  ) {
    $this->configFactory = $configFactory;
    $this->storageHandler = $storageHandler;
  }

  /**
   * Reacts to a config delete and checks if behaviors field should be removed.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The config crud event.
   */
  public function onConfigDelete(ConfigCrudEvent $event) {
    if (!$this->checkBehaviorConfig($event)) {
      return;
    }

    $bundles = $this->configFactory->getConfiguredBundlesByEntityType($this->entityTypeId);

    if (!empty($bundles)) {
      return;
    }

    $this->storageHandler->removeBaseFieldConfiguration($this->entityTypeId);
  }

  /**
   * Checks if we are dealing with a behavior config.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The config crud event.
   *
   * @return bool
   *   Are we dealing with a behavior config?
   */
  protected function checkBehaviorConfig(ConfigCrudEvent $event) {
    if (!$config = $event->getConfig()) {
      return FALSE;
    }

    if (!$this->configFactory->isBehaviorConfig($config->getName())) {
      return FALSE;
    }

    $entityTypeInfo = $this->configFactory->getEntityTypeAndBundleFromEditable(
      $config->getName()
    );

    if (!$entityTypeInfo) {
      return FALSE;
    }

    $this->config = $config;
    $this->entityTypeId = $entityTypeInfo[0];
    $this->bundle = $entityTypeInfo[1];
    return TRUE;
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::DELETE][] = ['onConfigDelete', 40];
    return $events;
  }

}
