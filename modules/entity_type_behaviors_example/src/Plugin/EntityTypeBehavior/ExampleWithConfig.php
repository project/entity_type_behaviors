<?php

namespace Drupal\entity_type_behaviors_example\Plugin\EntityTypeBehavior;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;

/**
 * Class Example.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id="example_with_config",
 *  description="This is an example behavior that also has config.",
 *  label=@Translation("Example With Config Behavior"),
 *  entityTypes={"node", "media"}
 * )
 */
class ExampleWithConfig extends EntityTypeBehaviorBase {

  /**
   * An array containing multiple options.
   *
   * @var array
   */
  protected $options = [
    'yellow' => 'Yellow',
    'green' => 'Green',
    'blue' => 'Blue',
  ];

  /**
   * {@inheritdoc}
   */
  public function getForm(): array {
    $configColors = $this->getConfigValue('colors') ?? [];

    if (!empty($configColors)) {
      $options = array_intersect_key($this->options, $configColors);
    }
    else {
      $options = $this->options;
    }

    $element['colors'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Select colors'),
      '#default_value' => $this->getValueByKey('colors') ?? [],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(): array {
    $element['colors'] = [
      '#type' => 'checkboxes',
      '#options' => $this->options,
      '#title' => $this->t('Only allow following colors'),
      '#default_value' => $this->getConfigValue('colors') ?? [],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageConfig(array $config, array $form, FormStateInterface $form_state): array {
    foreach ($config['colors'] as $key => $value) {
      if ($value === 0) {
        unset($config['colors'][$key]);
      }
    }

    return $config;
  }

}
