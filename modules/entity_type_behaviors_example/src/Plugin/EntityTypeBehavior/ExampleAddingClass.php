<?php

namespace Drupal\entity_type_behaviors_example\Plugin\EntityTypeBehavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;

/**
 * Class Example.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id="example_adding_class",
 *  description="This is an example behavior with the view callback.",
 *  label=@Translation("Example Behavior Adding Class")
 * )
 */
class ExampleAddingClass extends EntityTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(array $defaultValues = []): array {
    $element['add_class'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add class?'),
      '#default_value' => $this->getValueByKey('add_class') ?? FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function view(
    array &$build,
    EntityInterface $entity,
    EntityViewDisplayInterface $display,
    $view_mode,
  ) {
    if (!$this->getValueByKey('add_class')) {
      return;
    }

    $build['#attributes']['class'][] = 'example-behavior-has-added-class';
  }

}
