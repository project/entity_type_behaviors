<?php

namespace Drupal\entity_type_behaviors_example\Plugin\EntityTypeBehavior;

use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;

/**
 * Class Example.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id = "example_media_only",
 *  description = "This is a Media Only example behavior.",
 *  label = @Translation("Example Media Only Behavior"),
 *  entityTypes={"media"}
 * )
 */
class ExampleMediaOnly extends EntityTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(array $defaultValues = []): array {
    $element['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter an example text'),
      '#default_value' => $this->getValueByKey('text') ?? '',
    ];

    return $element;
  }

}
