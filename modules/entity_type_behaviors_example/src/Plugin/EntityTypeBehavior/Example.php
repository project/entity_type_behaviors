<?php

namespace Drupal\entity_type_behaviors_example\Plugin\EntityTypeBehavior;

use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;

/**
 * An example behavior.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id="example",
 *  description="This is an example behavior.",
 *  label=@Translation("Example Behavior")
 * )
 */
class Example extends EntityTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(array $defaultValues = []): array {
    $element['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter an example text'),
      '#default_value' => $this->getValueByKey('text') ?? '',
    ];

    return $element;
  }

}
