CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Developing a plugin
 * Installation
 * Configuration
 * Hooks
 * Developers
 * Maintainers

INTRODUCTION
------------
Entity Type Behaviors are plugins that can be enabled on
certain fieldable entity types.

These plugins can be used by content managers to configure display
options on a specific entity.

Each behavior has form callback, and the entered values of
that form will be stored in a blob base field on the entity. 

Developers can then use the values to change behaviors of the entity.

Good examples are eg:
- a behavior to configure background colors
- a behavior to configure the positioning of an image
- a behavior to configure margins on a paragraph
- ... 

The module provides easy hooks in which the build array 
can be altered.

Each plugin has the option to alter a plugin 
using the view method().
 
REQUIREMENTS
------------
- php >= 7.1
- Drupal >= 8.5

INSTALLATION
------------
Install the "Entity Type Behaviors" module as 
you would normally install a contributed Drupal module. 

Visit https://www.drupal.org/node/1897420 for further
information.

DEVELOPING A PLUGIN
-------------------
There is an entity_type_behaviors_example module, located in
modules/entity_type_behaviors_example

To create a plugin. Create a file in 
``src/Plugin/EntityTypeBehavior``

The following annotation is required:
```
@EntityTypeBehavior(
  id="example_with_config",
  description="This is an example behavior that also has config.",
  label=@Translation("Example With Config Behavior"),
  entityTypes={"node", "media"}
)
```

The entityTypes annotation will limit this plugin to only
entities of that type. Leaving this empty will
allow the plugin for any content type.

CONFIGURATION
-------------
Once you have a plugin for a specific entity type,
the behaviors can be enabled for any bundle of that 
entity type.

To do this, go to the entity type's edit form.
 
Normally, if you have plugins for that entity type,
a new form element should have appeared in additional settings.

This element is called "Behaviors". To enable behaviors,
toggle the checkbox "Enable behaviors on this entity type"

Select which behaviors you wish to enable.
And add some configuration.

Afterwards, export your Drupal site.
To learn how to export, go here for more information:
https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration

A new config file should now be available, eg:
entity_type_behaviors.entity_type_bundle.node.article

This config file contains the configuration of behaviors
for the entity type "node", with bundle "article".

If you look in your database, 
you'll notice that a new base field has been added to 
the entity type, called "behaviors"

This base field can be added anywhere on your form display and will
render a widget containing the form callbacks of the enabled
behavior plugins.

Content managers can enter values on the forms, and 
these will be stored as a blob in the "behaviors"
column.

HOOKS
-----
Some hooks have been made available to make preprocessing a bit easier.
These hooks can be found in ``entity_type_behaviors.api.php``

**hook_entity_type_behaviors_alter__BEHAVIOR**
```
This hook allows a user to preprocess any entity that has
an enabled behavior plugin of type BEHAVIOR.
EG: mymodule_entity_type_behaviors__preprocess__example_node_only
```

**hook_entity_type_behaviors_alter__BEHAVIOR__ENTITY_TYPE**
```
This hook allows a user to preprocess any entity that has
an enabled behavior plugin of type BEHAVIOR 
for a specific entity type ENTITY_TYPE.
EG: mymodule_entity_type_behaviors__preprocess__example_node_only__node
```

**hook_entity_type_behaviors_alter__BEHAVIOR__ENTITY_TYPE__BUNDLE**
```
This hook allows a user to preprocess any entity that has
an enabled behavior plugin of type BEHAVIOR 
for a specific entity type ENTITY_TYPE and a specific bundle.
EG: mymodule_entity_type_behaviors__preprocess__example_node_only__node__article
```

The same functions can be used in your .theme file. 
Simply change hook with the name of your theme.

DEVELOPERS
----------
* Maarten Heip - https://www.drupal.org/u/mheip

But a special thanks to Intracto, for allowing me to do this on company time.
https://www.intracto.com

MAINTAINERS
-----------
Current maintainers:
  * Maarten Heip - https://www.drupal.org/u/mheip
