<?php

namespace Drupal\Tests\entity_type_behaviors\Kernel;

use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\entity_test\Entity\EntityTestLabel;

/**
 * Class BehaviorConfigImportTest.
 *
 * This class will test if the installation of entity_type_behaviors configs
 * is processed correctly.
 *
 * @package Drupal\Tests\entity_type_behaviors\Kernel.
 *
 * @group entity_type_behaviors
 */
class BehaviorConfigImportTest extends FieldKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_type_behaviors',
    'entity_type_behaviors_example',
    'entity_test',
    'test_entity_type_behaviors_config_import',
  ];

  /**
   * The name of the field to use in this test.
   *
   * @var string
   */
  protected $fieldName = 'field_behavior_test';

  /**
   * The entity on which to do tests.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entity;

  /**
   * Another entity on which do tests.
   *
   * @var \Drupal\entity_test\Entity\EntityTestLabel
   */
  protected $anotherEntity;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['entity_type_behaviors']);

    // Install a second entity type.
    $this->installEntitySchema('entity_test_label');

    // Create an entity of each type, before importing the configuration.
    $this->entity = EntityTest::create();
    $this->entity->save();

    $this->anotherEntity = EntityTestLabel::create();
    $this->anotherEntity->save();

    // Install out test configuration.
    $this->installConfig(['test_entity_type_behaviors_config_import']);
  }

  /**
   * Tests the config import.
   */
  public function testConfigImport(): void {
    $context = [];
    $config_importer = $this->getMockBuilder('Drupal\Core\Config\ConfigImporter')
      ->disableOriginalConstructor()
      ->getMock();

    // Manually trigger the update for all newly entity_types with behaviors.
    entity_type_behaviors_update_behavior_entity_types($context, $config_importer);

    // Reload the entity to make sure it knows about the new field.
    $entity = \Drupal::entityTypeManager()->getStorage('entity_test')->load($this->entity->id());
    $another_entity = \Drupal::entityTypeManager()->getStorage('entity_test')->load($this->anotherEntity->id());

    // Check if the field has been installed.
    $this->assertTrue($entity->hasField('behaviors'));
    $this->assertTrue($another_entity->hasField('behaviors'));
  }

  /**
   * Tests if all configs have been installed by counting them.
   */
  public function testImportedConfig(): void {
    /** @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $configFactory */
    $configFactory = \Drupal::service('entity_type_behaviors.config.factory');
    $entityTypes = $configFactory->getConfiguredEntityTypesAndBundles();
    $this->assertCount(2, $entityTypes);
  }

}
