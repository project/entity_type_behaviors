<?php

namespace Drupal\Tests\entity_type_behaviors\Kernel;

use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\entity_type_behaviors\Plugin\Field\FieldType\EntityTypeBehaviorItem;

/**
 * Class FieldTypeSerializationTest.
 *
 * This class will test the functionality of our behaviors base field.
 * The way it is attached to an entity type.
 * The way data can be retrieved from it.
 *
 * @package Drupal\Tests\entity_type_behaviors\Unit.
 *
 * @group entity_type_behaviors
 */
class BehaviorFieldTest extends FieldKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_type_behaviors',
    'entity_type_behaviors_example',
    'entity_test',
  ];

  /**
   * The name of the field to use in this test.
   *
   * @var string
   */
  protected $fieldName = 'field_behavior_test';

  /**
   * Configuration schema can be incorrect.
   *
   * @var bool
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   */
  protected $strictConfigSchema = FALSE;

  /**
   * The entity on which to do tests.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['entity_type_behaviors']);

    $behaviorConfig = [
      'example' => [
        'enable' => 1,
      ],
    ];

    // Create entity type behavior configuration.
    $this->container
      ->get('entity_type_behaviors.config.factory')
      ->saveBehaviors('entity_test', 'entity_test', $behaviorConfig);

    // Add base field to "entity_test".
    $this->container
      ->get('entity_type_behaviors.storage.handler')
      ->updateBaseFieldConfiguration('entity_test');

    $this->entity = EntityTest::create([
      'behaviors' => $this->getBehaviors(),
    ]);

    $this->entity->save();
  }

  /**
   * Tests if the entity_test entity has a base field "behaviors".
   */
  public function testHasField() {
    $this->assertTrue($this->entity->hasField('behaviors'));
  }

  /**
   * Tests the getValue() function of a EntityTypeBehaviorItem.
   */
  public function testFieldType() {
    $behaviorField = $this->entity->get('behaviors')->first();
    $this->assertNotNull($behaviorField);

    $this->assertTrue($behaviorField instanceof EntityTypeBehaviorItem);

    /** @var \Drupal\entity_type_behaviors\Plugin\Field\FieldType\EntityTypeBehaviorItem $behaviorField */
    $unserializedValues = $behaviorField->getValue()['value'] ?? [];
    $this->assertEquals($unserializedValues, $this->getBehaviors());
    $this->assertArrayHasKey('background_color', $unserializedValues);
  }

  /**
   * Returns behaviors.
   *
   * @return array
   *   An array containing behaviors.
   */
  protected function getBehaviors() : array {
    return [
      'background_color' => [
        'background_color' => 'red',
      ],
    ];
  }

}
